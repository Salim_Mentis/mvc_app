﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace First_MVC_app.Models
{
    public class Customer
    {
        //Customer name and code should not be empty
        [Required]
        [RegularExpression("^[A-Z]{3,3}[0-9]{4,4}$")]
        public string CustomerCode { get; set; }

        [Required]
        [StringLength(10)]        
        public string CustomerName { get; set; }
    }
}