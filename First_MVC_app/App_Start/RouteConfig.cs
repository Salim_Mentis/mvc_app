﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace First_MVC_app
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.config");

            //routes.MapRoute(
            //    name: "GoToHome",
            //    url: "",
            //    defaults: new { controller = "Home", action = "GoToHome", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "GoToHome2",
                url: "Home/MyHomePage",
                defaults: new { controller = "Home", action = "GoToHome", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
